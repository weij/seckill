package com.ruyuan.seckill.domain.enums;

/**
 * 角色字义
 */
public enum Role {

    /**
     * 买家角色
     */
    BUYER,

    /**
     * 卖家角色
     */
    SELLER,
    /**
     * 店员角色
     */
    CLERK,

    /**
     * 管理员
     */
    ADMIN




}
