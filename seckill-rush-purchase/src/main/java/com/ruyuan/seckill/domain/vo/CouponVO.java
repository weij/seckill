package com.ruyuan.seckill.domain.vo;

import com.ruyuan.seckill.domain.MemberCoupon;

import java.io.Serializable;
import java.util.List;

/**
 * 优惠券
 */
public class CouponVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5236361119763282449L;

    /**
     * 记录下单时使用的会员优惠券ID
     */
    private Integer memberCouponId;

    /**
     * 记录下单时赠送的优惠券ID
     */
    private Integer couponId;
    /**
     * 卖家id
     */
    private Integer sellerId;
    /**
     * 卖家名称
     */
    private String sellerName;
    /**
     * 金额
     */
    private Double amount;
    /**
     * 有效期
     */
    private Long endTime;
    /**
     * 使用条件
     */
    private String useTerm;

    /**
     * 优惠券门槛金额
     */
    private Double couponThresholdPrice;
    /**
     * 是否可用，1为可用，0为不可用
     */
    private int enable;
    /**
     * 是否被选中，1为选中，0为不选中
     */
    private int selected;
    /**
     * 错误信息，结算页使用
     */
    private String errorMsg;
    /**
     * 优惠券使用范围
     */
    private String useScope;
    /**
     * 允许使用的ids
     */
    private List<Integer> enableSkuList;


    public CouponVO() {
    }

    public CouponVO(MemberCoupon memberCoupon) {

        this.setCouponId(memberCoupon.getCouponId());
        this.setAmount(memberCoupon.getCouponPrice());
        this.setUseTerm("满" + memberCoupon.getCouponThresholdPrice() + "可用");
        this.setSellerId(memberCoupon.getSellerId());
        this.setMemberCouponId(memberCoupon.getMcId());
        this.setEndTime(memberCoupon.getEndTime());
        this.setCouponThresholdPrice(memberCoupon.getCouponThresholdPrice());
        this.setUseScope(memberCoupon.getUseScope());
        this.setSellerName(memberCoupon.getSellerName());

    }

    public Integer getMemberCouponId() {
        return memberCouponId;
    }

    public void setMemberCouponId(Integer memberCouponId) {
        this.memberCouponId = memberCouponId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getUseTerm() {
        return useTerm;
    }

    public void setUseTerm(String useTerm) {
        this.useTerm = useTerm;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }


    public Double getCouponThresholdPrice() {
        return couponThresholdPrice;
    }

    public void setCouponThresholdPrice(Double couponThresholdPrice) {
        this.couponThresholdPrice = couponThresholdPrice;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getUseScope() {
        return useScope;
    }

    public void setUseScope(String useScope) {
        this.useScope = useScope;
    }

    public List<Integer> getEnableSkuList() {
        return enableSkuList;
    }

    public void setEnableSkuList(List<Integer> enableSkuList) {
        this.enableSkuList = enableSkuList;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    @Override
    public String toString() {
        return "CouponVO{" +
                "memberCouponId=" + memberCouponId +
                ", couponId=" + couponId +
                ", sellerId=" + sellerId +
                ", amount=" + amount +
                ", endTime=" + endTime +
                ", useTerm='" + useTerm + '\'' +
                ", couponThresholdPrice=" + couponThresholdPrice +
                ", enable=" + enable +
                ", selected=" + selected +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }


}
