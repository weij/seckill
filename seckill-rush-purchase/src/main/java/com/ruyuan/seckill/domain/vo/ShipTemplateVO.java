package com.ruyuan.seckill.domain.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ruyuan.seckill.domain.ShipTemplateDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 运费模板VO
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShipTemplateVO extends ShipTemplateDO implements Serializable {

    private static final long serialVersionUID = -1565652419452783173L;
    @ApiModelProperty(name = "items", value = "指定配送区域", required = true)
    private List<ShipTemplateChildBuyerVO>  items;

    @ApiModelProperty(name = "free_items", value = "指定配送区域", required = true)
    private List<ShipTemplateChildBuyerVO>  freeItems;

    public List<ShipTemplateChildBuyerVO> getItems() {
        return items;
    }

    public void setItems(List<ShipTemplateChildBuyerVO> items) {
        this.items = items;
    }

    public List<ShipTemplateChildBuyerVO> getFreeItems() {
        return freeItems;
    }

    public void setFreeItems(List<ShipTemplateChildBuyerVO> freeItems) {
        this.freeItems = freeItems;
    }

    @Override
    public String toString() {
        return "ShipTemplateVO{" +
                "items=" + items +
                '}';
    }
}
