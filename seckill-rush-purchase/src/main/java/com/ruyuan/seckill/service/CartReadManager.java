package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import com.ruyuan.seckill.domain.vo.CartView;

/**
 *
 * 购物车只读操作业务接口<br>
 * 包含对购物车读取操作
 */
public interface CartReadManager {


    /**
     * 读取购物车数据，并计算优惠和价格
     * @param way 获取方式
     * @return
     */
    CartView getCartListAndCountPrice(CheckedWay way);



    /**
     * 由缓存中取出已勾选的购物列表<br>
     * @param way 获取方式
     * @return
     */
    CartView getCheckedItems(CheckedWay way, Buyer buyer);


}
