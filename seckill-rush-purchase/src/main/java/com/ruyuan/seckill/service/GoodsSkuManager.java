package com.ruyuan.seckill.service;

import com.ruyuan.seckill.domain.vo.GoodsSkuVO;

/**
  * 商品sku业务层
 */

public interface GoodsSkuManager {
    /**
     * 缓存中查询sku信息
     *
     * @param skuId
     * @return
     */
    GoodsSkuVO getSkuFromCache(Integer skuId);

    /**
     * 查询sku信息
     *
     * @param skuId
     * @return
     */
    GoodsSkuVO getSku(Integer skuId);
}
