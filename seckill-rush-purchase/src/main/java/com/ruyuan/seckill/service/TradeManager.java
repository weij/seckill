package com.ruyuan.seckill.service;

import com.ruyuan.seckill.domain.dto.OrderDTO;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import com.ruyuan.seckill.domain.vo.TradeVO;

/**
 * 交易管理
 *
 */
public interface TradeManager {


    /**
     * 秒杀交易创建
     *
     * @param orderDTO 订单信息
     * @return
     */
    TradeVO seckillCreate(OrderDTO orderDTO);

    /**
     * 秒杀交易创建
     *
     * @param clientType 客户的类型
     * @param way        检查获取方式
     * @return 返回交易结果
     */
    TradeVO secKillCreate(String clientType, CheckedWay way,Integer uid);

}
