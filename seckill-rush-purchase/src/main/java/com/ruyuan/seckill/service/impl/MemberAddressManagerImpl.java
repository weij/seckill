package com.ruyuan.seckill.service.impl;

import com.ruyuan.seckill.cache.Cache;
import com.ruyuan.seckill.domain.MemberAddress;
import com.ruyuan.seckill.service.MemberAddressManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员地址业务类
 */
@Service
public class MemberAddressManagerImpl implements MemberAddressManager {



    @Autowired
    private Cache cache;



    /**
     * 获取会员地址
     *
     * @param id 会员地址主键
     * @return MemberAddress  会员地址
     */
    @Override
    public MemberAddress getModel(Integer id) {
        return (MemberAddress) cache.get(id + "::memberAddress");
    }

    /**
     * 获取会员默认地址
     *
     * @param memberId 会员id
     * @return 会员默认地址
     */
    @Override
    public MemberAddress getDefaultAddress(Integer memberId) {
        return (MemberAddress) cache.get(memberId+"::memberAddress");
    }
}
